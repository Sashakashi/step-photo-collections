package by.itstep.collectionsphotos.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name="users")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="name")
    private String name;

    @Column(name="login")
    private  String login;

    @Column(name="password")
    private String password;

    @Column(name="email")
    private String email;

    //@JsonIgnore
    @ToString.Exclude                   // чтобы не было цикличных зависимостей
    @EqualsAndHashCode.Exclude          // чтобы не было цикличных зависимостей
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<CollectionEntity> collections=new ArrayList<>();

    //@JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<CommentEntity> comments= new ArrayList<>();









}
