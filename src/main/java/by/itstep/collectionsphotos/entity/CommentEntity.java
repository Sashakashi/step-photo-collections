package by.itstep.collectionsphotos.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="comments")
public class CommentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="message")
    private String message;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="user_id")
    private UserEntity user;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="photo_id")
    private PhotoEntity photo;
















}
