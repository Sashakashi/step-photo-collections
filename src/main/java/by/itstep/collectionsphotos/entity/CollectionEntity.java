package by.itstep.collectionsphotos.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name="collections")
public class CollectionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name="description")
    private String description;
    @ManyToOne
    @JoinColumn(name="user_id")
    private UserEntity user;


    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany
    @JoinTable(name="collections_photos",
                joinColumns = {@JoinColumn(name="collection_id")},          //главная
                inverseJoinColumns = {@JoinColumn(name = "photo_id")} )
    private List<PhotoEntity> photos;



}
