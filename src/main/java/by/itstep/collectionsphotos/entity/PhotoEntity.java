package by.itstep.collectionsphotos.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="photos")
@Data
public class PhotoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "link")
    private String link;
    @Column(name = "name")
    private String name;
    @Column(name = "rating")
    private Integer rating;

    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "photos", fetch = FetchType.LAZY)
    private List<CollectionEntity> collections;

    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "photo", fetch = FetchType.LAZY)
    private List<CommentEntity> comments;



}
