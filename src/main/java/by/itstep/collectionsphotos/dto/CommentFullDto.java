package by.itstep.collectionsphotos.dto;
import by.itstep.collectionsphotos.entity.UserEntity;
import lombok.Data;

@Data
public class CommentFullDto {
    private Integer id;
    private String message;
    private UserShortDto user;
    private Integer photoId;
}
