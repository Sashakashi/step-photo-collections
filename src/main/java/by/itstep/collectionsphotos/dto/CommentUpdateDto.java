package by.itstep.collectionsphotos.dto;
import lombok.Data;

@Data
public class CommentUpdateDto {
    private Integer id;
    private String message;
}
