package by.itstep.collectionsphotos.dto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserFullDto {
    private Integer id;
    private String name;
    private String login;
    private String email;
    private List<CollectionShortDto> collections=new ArrayList<>();
}
