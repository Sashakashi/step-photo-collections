package by.itstep.collectionsphotos.dto;
import lombok.Data;

@Data
public class CollectionCreateDto {
    private String name;
    private String description;
    private  Integer userId;
}
