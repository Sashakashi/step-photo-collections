package by.itstep.collectionsphotos.dto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import lombok.Data;
import java.util.List;

@Data
public class PhotoFullDto {
    private Integer id;
    private String link;
    private String name;
    private Integer rating;
    private List<CommentFullDto> comments;
}
