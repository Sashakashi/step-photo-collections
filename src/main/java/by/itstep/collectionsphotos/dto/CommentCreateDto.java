package by.itstep.collectionsphotos.dto;
import lombok.Data;

@Data
public class CommentCreateDto {
    private String message;
    private Integer userId;
    private Integer photoId;
}
