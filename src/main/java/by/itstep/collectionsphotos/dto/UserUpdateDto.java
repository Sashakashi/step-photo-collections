package by.itstep.collectionsphotos.dto;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserUpdateDto {
    @NotNull(message = "Id can not be null")
    private Integer id;
    @NotBlank(message = "Name can not be blank")
    private String name;
    @NotBlank(message = "Login can not be blank")
    @Size(min = 3,max = 20, message = "Login length must be between 3 and 20")
    private String login;


}
