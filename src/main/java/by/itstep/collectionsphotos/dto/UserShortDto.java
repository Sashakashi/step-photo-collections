package by.itstep.collectionsphotos.dto;
import lombok.Data;
@Data
public class UserShortDto {

    private Integer id;
    private String name;
    private String login;
    private String email;


}
