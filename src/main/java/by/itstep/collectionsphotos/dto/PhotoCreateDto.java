package by.itstep.collectionsphotos.dto;
import lombok.Data;

@Data
public class PhotoCreateDto {
    private String link;
    private String name;
}
