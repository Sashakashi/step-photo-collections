package by.itstep.collectionsphotos.dto;
import lombok.Data;
import javax.validation.constraints.*;

@Data
public class UserCreateDto {

    @NotBlank(message = "Name can not be blank")
    private String name;
    @NotBlank(message = "Login can not be blank")
    @Size(min = 3,max = 20, message = "Login length must be between 3 and 20")
    private String login;
    @Email(message = "Email must have valid format")
    @NotEmpty(message = "Email can not be null")
    private String email;
    @NotBlank(message = "Password can not be blank")
    @Size(min = 8, max = 100, message = "Password length must be between 8 and 100")
    private String password;

}
