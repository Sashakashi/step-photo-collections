package by.itstep.collectionsphotos.dto;
import lombok.Data;

@Data
public class CollectionShortDto {
    private Integer id;
    private String name;
    private String description;
}
