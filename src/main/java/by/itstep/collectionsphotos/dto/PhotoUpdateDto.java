package by.itstep.collectionsphotos.dto;
import lombok.Data;

@Data
public class PhotoUpdateDto {
    private Integer id;
    private String name;
}
