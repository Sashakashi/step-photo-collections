package by.itstep.collectionsphotos.service;
import by.itstep.collectionsphotos.dto.CommentCreateDto;
import by.itstep.collectionsphotos.dto.CommentFullDto;
import by.itstep.collectionsphotos.dto.CommentUpdateDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.CommentMapper;
import by.itstep.collectionsphotos.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.PublicKey;
import java.util.List;
@Service
public class CommentService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CommentMapper commentMapper;


    public CommentFullDto findById(int id){
        CommentEntity comment = commentRepository.findById(id);
        if(comment==null){
            throw new RuntimeException("CommentService -> Comment not found by id: "+id);
        }
        System.out.println("CommentService -> Found comment: "+comment);
        CommentFullDto commentDto = commentMapper.map(comment);
        return commentDto;
    }

    public List<CommentFullDto> findAll(){
        List<CommentEntity> allComments = commentRepository.findAll();
        System.out.println("CommentService -> Found comments: "+allComments);
        List<CommentFullDto> allDtos = commentMapper.map(allComments);
        return allDtos;
    }

    public CommentFullDto create(CommentCreateDto createRequest){
        CommentEntity comment = commentMapper.map(createRequest);
        UserEntity user = userRepository.findById(createRequest.getUserId());
        PhotoEntity photo = photoRepository.findById(createRequest.getPhotoId());
        comment.setUser(user);
        comment.setPhoto(photo);
        commentRepository.create(comment);
        System.out.println("CommentService -> Created comment: "+comment);
        CommentFullDto createdDto = commentMapper.map(comment);
        return createdDto;
    }

    public CommentFullDto update(CommentUpdateDto updateRequest){
        if(updateRequest.getId()==null){
            throw new RuntimeException("CommentService -> Can not update entity without id");
        }
        if(commentRepository.findById(updateRequest.getId())==null){
            throw new RuntimeException("CommentService -> Comment not found by id: "+updateRequest.getId());
        }
        CommentEntity comment = commentRepository.findById(updateRequest.getId());
        comment.setMessage(updateRequest.getMessage());
        CommentEntity updatedComment = commentRepository.update(comment);
        System.out.println("CommentService -> Updated comment: "+comment);
        CommentFullDto updatedDto = commentMapper.map(updatedComment);
        return updatedDto;
    }

    public void deleteById(int id){
        CommentEntity commentToDelete = commentRepository.findById(id);
        if(commentToDelete==null){
            throw new RuntimeException("CommentService -> Comment was not found by id: "+ id+
                    " and can not be deleted.");
        }
        commentRepository.deleteById(id);
        System.out.println("CommentService -> Deleted comment: "+commentToDelete);
    }



}
