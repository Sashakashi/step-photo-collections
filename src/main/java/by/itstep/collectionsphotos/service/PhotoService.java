package by.itstep.collectionsphotos.service;
import by.itstep.collectionsphotos.dto.PhotoCreateDto;
import by.itstep.collectionsphotos.dto.PhotoFullDto;
import by.itstep.collectionsphotos.dto.PhotoShortDto;
import by.itstep.collectionsphotos.dto.PhotoUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.mapper.PhotoMapper;
import by.itstep.collectionsphotos.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PhotoService {

    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CollectionService collectionService;
    @Autowired
    private PhotoMapper photoMapper;

    public PhotoFullDto findById(int id){
        PhotoEntity photo = photoRepository.findById(id);
        if(photo==null){
            throw new RuntimeException("PhotoService -> Photo not found by id: "+ id);
        }
        System.out.println("PhotoService -> Found photo: "+ photo);
        PhotoFullDto dto = photoMapper.map(photo);
        return  dto;
    }

    public List<PhotoShortDto> findAll(){
        List<PhotoEntity> allPhotos = photoRepository.findAll();
        System.out.println("PhotoService -> Found photos: "+ allPhotos);
        List<PhotoShortDto> allDtos = photoMapper.map(allPhotos);
        return allDtos;
    }

    public PhotoFullDto create(PhotoCreateDto createRequest){
//        if(photo.getId()!=null){
//            throw new RuntimeException("PhotoService -> Can not create photo with id.");
//        }
        PhotoEntity photo = photoMapper.map(createRequest);
        PhotoEntity createdPhoto = photoRepository.create(photo);
        System.out.println("PhotoService -> Created photo: "+ photo);
        PhotoFullDto dto = photoMapper.map(createdPhoto);
        return dto;
    }

    public PhotoFullDto update(PhotoUpdateDto updateRequest){
        if(updateRequest.getId()==null){
            throw new RuntimeException("PhotoService -> Can not update photo without id.");
        }
        if(photoRepository.findById(updateRequest.getId())==null){
            throw new RuntimeException("PhotoService -> Photo is not found by id: "+updateRequest.getId());
        }
        PhotoEntity photoToUpdate = photoRepository.findById(updateRequest.getId());
        photoToUpdate.setName(updateRequest.getName());
        PhotoEntity updatedPhoto = photoRepository.update(photoToUpdate);
        System.out.println("PhotoService -> Updated photo: "+ updatedPhoto);
        PhotoFullDto updatedDto = photoMapper.map(updatedPhoto);
        return updatedDto;
    }

    public void deleteById(int photoId){
        PhotoEntity photoToDelete = photoRepository.findById(photoId);
        if(photoToDelete==null){
            throw new RuntimeException("PhotoService -> Photo not found by id: "+ photoId);
        }

        List<CommentEntity> commentsToPhoto = photoToDelete.getComments();
        for(CommentEntity comment:commentsToPhoto){
            commentRepository.deleteById(comment.getId());
        }

        List<CollectionEntity> collectionsWithPhoto = photoToDelete.getCollections();
        for(CollectionEntity collection:collectionsWithPhoto){
            collectionService.removePhoto(collection.getId(),photoId);
        }

        photoRepository.deleteById(photoId);
        System.out.println("PhotoService -> Deleted photo: "+ photoToDelete);
    }





}
