package by.itstep.collectionsphotos.service;
import by.itstep.collectionsphotos.dto.CollectionCreateDto;
import by.itstep.collectionsphotos.dto.CollectionFullDto;
import by.itstep.collectionsphotos.dto.CollectionShortDto;
import by.itstep.collectionsphotos.dto.CollectionUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.CollectionMapper;
import by.itstep.collectionsphotos.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CollectionService {

    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CollectionMapper collectionMapper;


    public CollectionFullDto findById(int id){
        CollectionEntity collection = collectionRepository.findById(id);
        if(collection==null){
            throw new RuntimeException("CollectionService -> Collection not found by id:"+id);
        }
        System.out.println("CollectionService -> Found Collection: "+collection);
        CollectionFullDto dto = collectionMapper.map(collection);
        return dto;
    }

    public List<CollectionShortDto> findAll(){
        List<CollectionEntity> allCollections = collectionRepository.findAll();
        System.out.println("CollectionService -> Found users: "+ allCollections);
        List<CollectionShortDto> allDtos = collectionMapper.map(allCollections);
        return allDtos;
    }

    public CollectionFullDto create(CollectionCreateDto createRequest){
        UserEntity user = userRepository.findById(createRequest.getUserId());
//        if(collection.getId()!=null){
//            throw new RuntimeException("CollectionService -> Can not create collection with id.");
//        }
        if(user==null){
            throw new RuntimeException("CollectionService -> Can not create collection without user or userId.");
        }
        CollectionEntity collection = collectionMapper.map(createRequest);
        collection.setUser(user);
        CollectionEntity createdCollection = collectionRepository.create(collection);
        System.out.println("CollectionService -> Created collection: "+ collection);
        CollectionFullDto createdDto = collectionMapper.map(createdCollection);
        return createdDto;
    }


    public CollectionFullDto update(CollectionUpdateDto updateRequest){
        CollectionEntity collectionToUpdate = collectionRepository.findById(updateRequest.getId());
        if(updateRequest.getId()==null){
            throw new RuntimeException("CollectionService -> Can not update collection without id.");
        }
        if(collectionToUpdate==null){
            throw new RuntimeException("CollectionService -> Collection is not found by id: "+updateRequest.getId());
        }

        collectionToUpdate.setName(updateRequest.getName());
        collectionToUpdate.setDescription(updateRequest.getDescription());
        CollectionEntity updatedCollection = collectionRepository.update(collectionToUpdate);
        System.out.println("CollectionService -> Updated collection: "+ collectionToUpdate);
        CollectionFullDto updatedDto = collectionMapper.map(updatedCollection);
        return updatedDto;
    }

    public void deleteById(int id){
        CollectionEntity collectionToDelete = collectionRepository.findById(id);
        if(collectionToDelete==null){
            throw new RuntimeException("CollectionService -> Collection was not found by id: "+ id+
                    " and can not be deleted.");
        }
        collectionRepository.deleteById(id);
        System.out.println("CollectionService -> Deleted collection: "+collectionToDelete);
    }


    public void addPhoto(int collectionId, int photoId){
        CollectionEntity collectionToUpdate = collectionRepository.findById(collectionId);
        List<PhotoEntity> photosFromCollection = collectionToUpdate.getPhotos();
        photosFromCollection.add(photoRepository.findById(photoId));
        collectionRepository.update(collectionToUpdate);
    }

    public void removePhoto(int collectionId, int photoId){
        CollectionEntity collectionToUpdate = collectionRepository.findById(collectionId);
        List<PhotoEntity> photosFromCollection = collectionToUpdate.getPhotos();
        PhotoEntity photoToDelete = photoRepository.findById(photoId);
        if (!photosFromCollection.contains(photoToDelete)){
            throw new RuntimeException("There is no photo with id: "+ photoId+
                    " in collection with id: "+ collectionId);
        }
        photosFromCollection.remove(photoToDelete);
        collectionRepository.update(collectionToUpdate);
    }





}
