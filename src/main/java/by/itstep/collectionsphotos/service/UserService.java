package by.itstep.collectionsphotos.service;
import by.itstep.collectionsphotos.config.SecurityService;
import by.itstep.collectionsphotos.dto.UserCreateDto;
import by.itstep.collectionsphotos.dto.UserShortDto;
import by.itstep.collectionsphotos.dto.UserFullDto;
import by.itstep.collectionsphotos.dto.UserUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.UserMapper;
import by.itstep.collectionsphotos.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private SecurityService securityService;


    public UserFullDto findById(int id){
        UserEntity user = userRepository.findById(id);
        if(user==null){
            throw new RuntimeException("UserService ->User not found by id: "+ id);
        }
        System.out.println("UserService -> Found user: "+ user);
        UserFullDto dto = userMapper.map(user);
        return dto;
    }

    public List<UserShortDto> findAll(){
        List<UserEntity> allUsers = userRepository.findAll();
        System.out.println("UserService -> Found users: "+ allUsers);
        List<UserShortDto> allDtos = userMapper.map(allUsers);
        return allDtos;
    }

    public UserFullDto create(UserCreateDto createRequest){
        UserEntity user = userMapper.map(createRequest);
//        if(user.getId()!=null){                                                                unnecessary
//            throw new RuntimeException("UserService -> Can not create user with id.");
//        }
        List<UserEntity> existingUsers = userRepository.findAll();
        for(UserEntity u: existingUsers){
            if(u.getEmail().equalsIgnoreCase(user.getEmail())){
                throw new RuntimeException("UserService -> Email: "+ user.getEmail()+" is taken!");
            }
        }
        UserEntity createdUser = userRepository.create(user);
        System.out.println("UserService -> Created user: "+ user);
        UserFullDto createdDto = userMapper.map(createdUser);
        return  createdDto;
    }


    public UserFullDto update(UserUpdateDto updateRequest){
        UserEntity currentUser = securityService.getAuthenticatedUser();
        if(!currentUser.getId().equals(updateRequest.getId())){
            throw new RuntimeException("UserService -> Can not update another user");
        }
//        if(updateRequest.getId()==null){
//            throw new RuntimeException("UserService -> Can not update entity without id");
//        }
        if(userRepository.findById(updateRequest.getId())==null){
            throw new RuntimeException("UserService -> User not found by id:"+ updateRequest.getId());
        }
        UserEntity user = userRepository.findById(updateRequest.getId());
        user.setLogin(updateRequest.getLogin());
        user.setName(updateRequest.getName());

        UserEntity updatedUser = userRepository.update(user);
        System.out.println("UserService -> Updated user: "+ user);
        UserFullDto updatedDto = userMapper.map(updatedUser);
        return updatedDto;
    }


    public void deleteById(int id){
        UserEntity userToDelete = userRepository.findById(id);
        if(userToDelete==null){
            throw new RuntimeException("UserService -> User wasn't found by id: "+id+
                    " and can not be deleted.");
        }

        List<CollectionEntity> existingCollections = userToDelete.getCollections();
        List<CommentEntity> existingComments = userToDelete.getComments();
        for(CollectionEntity c: existingCollections){
            collectionRepository.deleteById(c.getId());
        }
        for(CommentEntity c: existingComments){
            commentRepository.deleteById(c.getId());
        }
        userRepository.deleteById(id);
        System.out.println("UserService -> Deleted user: "+ userToDelete);
    }

}
