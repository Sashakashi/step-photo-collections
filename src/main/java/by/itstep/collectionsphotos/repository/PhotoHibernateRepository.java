package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.utils.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class PhotoHibernateRepository implements PhotoRepository {

    @Override
    public PhotoEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        PhotoEntity foundPhoto = em.find(PhotoEntity.class, id);
        if(foundPhoto!=null){
            Hibernate.initialize(foundPhoto.getComments());
            Hibernate.initialize(foundPhoto.getCollections());
            for(CollectionEntity collection:foundPhoto.getCollections()){
                Hibernate.initialize(collection.getPhotos());
            }
        }
        em.getTransaction().commit();
        em.close();
        return foundPhoto;
    }

    @Override
    public List<PhotoEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        List <PhotoEntity> allPhotos= em.createNativeQuery("SELECT * FROM photos", PhotoEntity.class).getResultList();
        em.getTransaction().commit();
        em.close();
        return allPhotos;
    }

    @Override
    public PhotoEntity create(PhotoEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        em.persist(entity);
        em.getTransaction().commit();
        em.close();
        return entity;
    }



    @Override
    public PhotoEntity update(PhotoEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        em.merge(entity);
        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public void deleteById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        PhotoEntity entityToDelete = em.find(PhotoEntity.class, id);
        em.remove(entityToDelete);
        em.getTransaction().commit();
        em.close();
    }
    @Override
    public void deleteAll(){
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM photos").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
