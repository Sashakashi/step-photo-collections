package by.itstep.collectionsphotos.repository;
import antlr.preprocessor.Hierarchy;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.utils.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class UserHibernateRepository implements UserRepository{


    @Override
    public UserEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        UserEntity foundUser = em.find(UserEntity.class, id);
        //foundUser.getCollections().size();                  // чтобы загрузил коллекции при lazy
        if(foundUser!=null) {
            Hibernate.initialize(foundUser.getCollections());
            Hibernate.initialize(foundUser.getComments());
        }                                                       // чтобы загрузил коллекции при lazy или так
        em.getTransaction().commit();
        em.close();
        return foundUser;
    }

    @Override
    public List<UserEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        //List <UserEntity> allUsers= em.createQuery("SELECT ue FROM UserEntity ue", UserEntity.class).getResultList();
        List <UserEntity> allUsers= em.createNativeQuery("SELECT * FROM users", UserEntity.class).getResultList();
        for(UserEntity user:allUsers){
            Hibernate.initialize(user.getCollections());
            Hibernate.initialize(user.getComments());
        }
        em.getTransaction().commit();
        em.close();
        return allUsers;
    }

    @Override
    public UserEntity create(UserEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        em.persist(entity);
        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public UserEntity update(UserEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        UserEntity entityToUpdate = em.find(UserEntity.class, entity.getId());
        entityToUpdate.setName(entity.getName());
        entityToUpdate.setLogin(entity.getLogin());
        entityToUpdate.setPassword(entity.getPassword());
        entityToUpdate.setEmail(entity.getEmail());
        Hibernate.initialize(entityToUpdate.getCollections());
        em.getTransaction().commit();
        em.close();
        return entityToUpdate;
    }

    @Override
    public void deleteById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        UserEntity entityToDelete = em.find(UserEntity.class, id);
        em.remove(entityToDelete);
        em.getTransaction().commit();
        em.close();
    }
    @Override
    public void deleteAll(){
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM users").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public UserEntity findByByLogin(String login) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        //String sql = "SELECT * FROM users WHERE login = '?';";
        String sql = String.format("SELECT * FROM users WHERE login = '%s';", login);
        UserEntity foundUser = (UserEntity) em.createNativeQuery(sql,UserEntity.class).getSingleResult();
        em.getTransaction().commit();
        em.close();
        return foundUser;
    }
}
