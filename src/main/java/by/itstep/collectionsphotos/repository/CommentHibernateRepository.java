package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.utils.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;


@Repository
public class CommentHibernateRepository implements CommentRepository {

    @Override
    public CommentEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        CommentEntity foundComment = em.find(CommentEntity.class, id);
        em.getTransaction().commit();
        em.close();
        return foundComment;
    }

    @Override
    public List<CommentEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        List <CommentEntity> allComments= em.createNativeQuery("SELECT * FROM comments", CommentEntity.class).getResultList();
        em.getTransaction().commit();
        em.close();
        return allComments;
    }

    @Override
    public CommentEntity create(CommentEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        em.persist(entity);
        em.getTransaction().commit();
        em.close();
        return entity;
    }




    @Override
    public CommentEntity update(CommentEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        em.merge(entity);
        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public void deleteById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        CommentEntity entityToDelete = em.find(CommentEntity.class, id);
        em.remove(entityToDelete);
        em.getTransaction().commit();
        em.close();
    }
    @Override
    public void deleteAll(){
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM comments").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
