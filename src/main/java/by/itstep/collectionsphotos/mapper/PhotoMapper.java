package by.itstep.collectionsphotos.mapper;
import by.itstep.collectionsphotos.dto.*;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PhotoMapper {

    @Autowired
    private CommentMapper commentMapper;

    public PhotoFullDto map(PhotoEntity entity){
        PhotoFullDto dto = new PhotoFullDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setLink(entity.getLink());
        dto.setRating(entity.getRating());
        List<CommentFullDto> commentsDtos =  commentMapper.map(entity.getComments());
        dto.setComments(commentsDtos);
        return dto;
    }

    public List <PhotoShortDto> map(List<PhotoEntity> entities){
        List<PhotoShortDto> dtos = new ArrayList<>();
        for(PhotoEntity entity:entities){
            PhotoShortDto dto = new PhotoShortDto();
            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setLink(entity.getLink());
            dto.setRating(entity.getRating());
            dtos.add(dto);
        }
        return dtos;
    }

    public PhotoEntity map(PhotoCreateDto createRequest){
        PhotoEntity entity = new PhotoEntity();
        entity.setName(createRequest.getName());
        entity.setLink(createRequest.getLink());
        return entity;
    }




}
