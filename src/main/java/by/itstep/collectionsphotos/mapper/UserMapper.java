package by.itstep.collectionsphotos.mapper;
import by.itstep.collectionsphotos.dto.CollectionShortDto;
import by.itstep.collectionsphotos.dto.UserCreateDto;
import by.itstep.collectionsphotos.dto.UserShortDto;
import by.itstep.collectionsphotos.dto.UserFullDto;
import by.itstep.collectionsphotos.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;
@Component
public class UserMapper {
    @Autowired
    private CollectionMapper collectionMapper;

    public UserFullDto map(UserEntity entity){
        UserFullDto dto = new UserFullDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        List<CollectionShortDto> collectionDtos = collectionMapper.map(entity.getCollections());
        dto.setCollections(collectionDtos);
        dto.setLogin(entity.getLogin());
        dto.setEmail(entity.getEmail());
        return dto;
    }

    public List<UserShortDto> map(List<UserEntity> entities){
        List<UserShortDto> dtos = new ArrayList<>();
        for(UserEntity entity:entities){
            UserShortDto dto = new UserShortDto();
            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setLogin(entity.getLogin());
            dto.setEmail(entity.getEmail());
            dtos.add(dto);
        }
        return dtos;
    }

    public UserEntity map(UserCreateDto dto){
        UserEntity entity = new UserEntity();
        entity.setName(dto.getName());
        entity.setEmail(dto.getEmail());
        entity.setLogin(dto.getLogin());
        entity.setPassword(dto.getPassword());
        return entity;
    }

    public UserShortDto mapToShort(UserEntity entity){
        UserShortDto dto = new UserShortDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setEmail(entity.getEmail());
        dto.setLogin(entity.getLogin());
        return dto;
    }


}
