package by.itstep.collectionsphotos.mapper;
import by.itstep.collectionsphotos.dto.CommentCreateDto;
import by.itstep.collectionsphotos.dto.CommentFullDto;
import by.itstep.collectionsphotos.dto.UserShortDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.repository.PhotoRepository;
import by.itstep.collectionsphotos.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CommentMapper {

    @Autowired
    private UserMapper userMapper;


    public CommentFullDto map(CommentEntity entity){
        CommentFullDto dto = new CommentFullDto();
        dto.setId(entity.getId());
        dto.setMessage(entity.getMessage());
        dto.setPhotoId(entity.getPhoto().getId());
        UserShortDto userDto = userMapper.mapToShort(entity.getUser());
        dto.setUser(userDto);
        return dto;
    }

    public List<CommentFullDto> map(List<CommentEntity> entities){
        List<CommentFullDto> dtos = new ArrayList<>();
        for(CommentEntity entity:entities){
            CommentFullDto dto = new CommentFullDto();
            dto.setId(entity.getId());
            dto.setMessage(entity.getMessage());
            dto.setPhotoId(entity.getPhoto().getId());
            UserShortDto userDto = userMapper.mapToShort(entity.getUser());
            dto.setUser(userDto);
            dtos.add(dto);
        }
        return dtos;
    }


    public CommentEntity map(CommentCreateDto dto){
        CommentEntity entity = new CommentEntity();
        entity.setMessage(dto.getMessage());
        return entity;
    }



}
