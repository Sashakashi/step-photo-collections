package by.itstep.collectionsphotos.mapper;
import by.itstep.collectionsphotos.dto.CollectionCreateDto;
import by.itstep.collectionsphotos.dto.CollectionFullDto;
import by.itstep.collectionsphotos.dto.CollectionShortDto;
import by.itstep.collectionsphotos.dto.PhotoShortDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

@Component
public class CollectionMapper {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private PhotoMapper photoMapper;


    public List<CollectionShortDto> map(List<CollectionEntity> entities){
        List<CollectionShortDto> dtos = new ArrayList<>();
        for(CollectionEntity entity: entities){
            CollectionShortDto dto = new CollectionShortDto();
            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setDescription(entity.getDescription());
            dtos.add(dto);
        }
        return dtos;
    }

    public CollectionFullDto map(CollectionEntity entity){
        CollectionFullDto dto = new CollectionFullDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        List<PhotoShortDto> shortPhotos = photoMapper.map(entity.getPhotos());
        dto.setPhotos(shortPhotos);
        dto.setUserId(entity.getUser().getId());
        return dto;
    }

    public CollectionEntity map(CollectionCreateDto createRequest){
        CollectionEntity entity = new CollectionEntity();
        entity.setName(createRequest.getName());
        entity.setDescription(createRequest.getDescription());
        return entity;
    }




}
