package by.itstep.collectionsphotos.config;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class UserAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String login = authentication.getName();
        String password = authentication.getCredentials().toString();

        System.out.println("Username: "+ login);
        System.out.println("Password: "+password);

        UserEntity foundUser = userRepository.findByByLogin(login);
        if(foundUser!=null && foundUser.getPassword().equals(password)){
            return new UsernamePasswordAuthenticationToken(login, password, new ArrayList<>());
        }else{
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }
}
