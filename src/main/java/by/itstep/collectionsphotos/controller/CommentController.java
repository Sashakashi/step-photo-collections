package by.itstep.collectionsphotos.controller;
import by.itstep.collectionsphotos.dto.CommentCreateDto;
import by.itstep.collectionsphotos.dto.CommentFullDto;
import by.itstep.collectionsphotos.dto.CommentUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.repository.CommentRepository;
import by.itstep.collectionsphotos.service.CollectionService;
import by.itstep.collectionsphotos.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
public class CommentController {
    @Autowired
    private CommentService commentService;

    @ResponseBody
    @RequestMapping(value = "/comments", method = RequestMethod.GET)
    public List<CommentFullDto> findAllComments(){
        List<CommentFullDto> allComments = commentService.findAll();
        return  allComments;
    }

    @ResponseBody
    @RequestMapping(value = "/comments/{id}", method = RequestMethod.GET)
    public CommentFullDto findById(@PathVariable int id){
        CommentFullDto comment = commentService.findById(id);
        return comment;
    }

    @ResponseBody
    @RequestMapping(value = "comments", method = RequestMethod.POST)
    public CommentFullDto create(@RequestBody CommentCreateDto createRequest){
        CommentFullDto createdComment = commentService.create(createRequest);
        return createdComment;
    }


    @ResponseBody
    @RequestMapping(value = "comments", method = RequestMethod.PUT)
    public CommentFullDto update( @RequestBody CommentUpdateDto updateRequest){
        CommentFullDto updatedComment = commentService.update(updateRequest);
        return updatedComment;
    }

    @ResponseBody
    @RequestMapping(value = "/comments/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id){
        commentService.deleteById(id);
    }



}
