package by.itstep.collectionsphotos.controller;
import by.itstep.collectionsphotos.dto.UserCreateDto;
import by.itstep.collectionsphotos.dto.UserShortDto;
import by.itstep.collectionsphotos.dto.UserFullDto;
import by.itstep.collectionsphotos.dto.UserUpdateDto;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    //@GetMapping("/users")
    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<UserShortDto> findAllUsers(){
        List<UserShortDto> allUsers = userService.findAll();
        return allUsers;
    }

    @ResponseBody
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public UserFullDto findById(@PathVariable int id){
        UserFullDto user = userService.findById(id);
        return user;
    }

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public UserFullDto create(@Valid @RequestBody UserCreateDto createRequest){
        UserFullDto createdUser = userService.create(createRequest);
        return createdUser;
    }

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.PUT)
    public UserFullDto update(@Valid @RequestBody UserUpdateDto updateRequest){
        UserFullDto updatedUser = userService.update(updateRequest);
        return updatedUser;
    }

    @ResponseBody
    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id){
        userService.deleteById(id);
    }



}
