package by.itstep.collectionsphotos.utils;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;

public class EntityUtils {

    public static UserEntity prepareUser(){
        UserEntity user = new UserEntity();
        user.setName("Name#"+Math.random());
        user.setPassword("Pass"+Math.random());
        user.setEmail(Math.random()+"@gmail.com");
        user.setLogin("Login#"+Math.random());
        return user;
    }

    public static PhotoEntity preparePhoto(){
        PhotoEntity photo = new PhotoEntity();
        photo.setLink("http://"+ Math.random());
        photo.setName("Photo#" + Math.random());
        photo.setRating(0);
        return photo;
    }

    public static CommentEntity prepareComment(PhotoEntity photo, UserEntity user){
        CommentEntity comment = new CommentEntity();
        comment.setMessage("Message#"+ Math.random());
        comment.setPhoto(photo);
        comment.setUser(user);
        return comment;
    }

    public static CollectionEntity prepareCollection(UserEntity user){
        CollectionEntity collection = new CollectionEntity();
        collection.setDescription("Description#"+ Math.random());
        collection.setName("Name#"+ Math.random());
        collection.setUser(user);
        return collection;
    }

}
