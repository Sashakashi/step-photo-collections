package by.itstep.collectionsphotos.utils;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
@Component
public class DatabaseCleaner {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CollectionRepository collectionRepository;


    public void clean(){
        for(CollectionEntity collection:collectionRepository.findAll()){            //чтобы таблица many-to-many стала пустой
            collection.setPhotos(new ArrayList<>());
            collectionRepository.update(collection);
        }
        collectionRepository.deleteAll();
        commentRepository.deleteAll();
        photoRepository.deleteAll();
        userRepository.deleteAll();
    }
}
