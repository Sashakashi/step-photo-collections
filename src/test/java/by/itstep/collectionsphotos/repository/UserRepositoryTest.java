package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.utils.DatabaseCleaner;
import by.itstep.collectionsphotos.utils.EntityUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class UserRepositoryTest {


    private UserRepository userRepository;
    private DatabaseCleaner cleaner;

    @BeforeEach
    public void setUp(){
        userRepository = new UserHibernateRepository();
        cleaner = new DatabaseCleaner();
        cleaner.clean();
    }

    @AfterEach
    public void clean(){
        cleaner.clean();
    }

    @Test
    public void create_happyPath(){
        //given
        UserEntity user = EntityUtils.prepareUser();
        //when
        UserEntity createdUser = userRepository.create(user);
        //then
        Assertions.assertNotNull(createdUser.getId());
        UserEntity foundUser = userRepository.findById(createdUser.getId());
        Assertions.assertEquals(user.getName(),foundUser.getName());
        Assertions.assertEquals(user.getPassword(),foundUser.getPassword());
        Assertions.assertEquals(user.getEmail(),foundUser.getEmail());
        Assertions.assertEquals(user.getLogin(),foundUser.getLogin());
    }

    @Test
    public void findAll_whenNoOneFound(){
        //given
        //when
       List<UserEntity> foundUsers = userRepository.findAll();
        //then
        Assertions.assertTrue(foundUsers.isEmpty());
    }

    @Test
    public void findAll_happyPath(){
        //given
        addUserToDb();
        addUserToDb();
        //when
        List<UserEntity> foundUsers = userRepository.findAll();
        //then
        Assertions.assertEquals(2,foundUsers.size());
    }
    @Test
    public void update_happyPath(){
        //given
        UserEntity existingUser = addUserToDb();
        existingUser.setEmail("updated");
        existingUser.setLogin("updated");
        existingUser.setPassword("updated");
        //when
        UserEntity updatedUser = userRepository.update(existingUser);
        //then
        Assertions.assertEquals(existingUser.getId(), updatedUser.getId());
        UserEntity foundUser = userRepository.findById(existingUser.getId());
        Assertions.assertEquals(existingUser.getName(),foundUser.getName());
        Assertions.assertEquals(existingUser.getPassword(),foundUser.getPassword());
        Assertions.assertEquals(existingUser.getEmail(),foundUser.getEmail());
        Assertions.assertEquals(existingUser.getLogin(),foundUser.getLogin());
    }
    @Test
    public void delete_happyPath(){
        //given
        UserEntity existingUser = addUserToDb();
        //when
        userRepository.deleteById(existingUser.getId());
        //then
        UserEntity foundUser = userRepository.findById(existingUser.getId());
        Assertions.assertNull(foundUser);
    }


    private UserEntity addUserToDb(){
        UserEntity userToAdd = EntityUtils.prepareUser();
        return userRepository.create(userToAdd);
    }




}
