package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.*;
import by.itstep.collectionsphotos.utils.DatabaseCleaner;
import by.itstep.collectionsphotos.utils.EntityUtils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

@SpringBootTest
public class PhotoServiceTest {

    private DatabaseCleaner dbCleaner;
    private PhotoService photoService;
    private PhotoRepository photoRepository;
    private UserRepository userRepository;
    private CollectionRepository collectionRepository;

    @BeforeEach
    public void setUp(){
        photoService = new PhotoService();
        dbCleaner = new DatabaseCleaner();
        photoRepository = new PhotoHibernateRepository();
        userRepository = new UserHibernateRepository();
        collectionRepository = new CollectionHibernateRepository();
        dbCleaner.clean();
    }

    @Test
    public void deleteById_happyPath(){
        //given
        UserEntity user = EntityUtils.prepareUser();
        userRepository.create(user);
        CollectionEntity collection = EntityUtils.prepareCollection(user);
        collectionRepository.create(collection);
        PhotoEntity photo = EntityUtils.preparePhoto();
        photoRepository.create(photo);
        collection.setPhotos(Arrays.asList(photo));
        collectionRepository.update(collection);
        //when
        photoService.deleteById(photo.getId());
        //then
        Assertions.assertNotNull(userRepository.findById(user.getId()));
        Assertions.assertNotNull(collectionRepository.findById(collection.getId()));
        Assertions.assertNull(photoRepository.findById(photo.getId()));
    }
}
